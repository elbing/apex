![APEX](https://harvey-os.org/images/harvey-printf.png)     
## APEX: the ANSI/POSIX Environment for Harvey OS

[![LicenseGPL](https://img.shields.io/badge/license-GPL-blue.svg)](https://bitbucket.org/elbing/apex/src/master/LICENSE)
[![LicenseMIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://bitbucket.org/elbing/apex/src/master/LICENSE.mit)

Welcome to Harvey's ANSI/POSIX Environment neXt subsystem, called APEX. We are delighted that you are interested in this
part of the Harvey OS project.

## What is APEX?

APEX is an ANSI C library POSIX compliant through [Musl libc code](http://www.musl-libc.org/) that
provides an easy way for building and running standard C programs in Harvey OS, being essential
for the main toolset of the system such as compilers (GCC and Clang).

Probably you already know about Harvey's resources, so the following are more detailed about APEX:

- Take a look at the
  [Getting Started](https://bitbucket.org/elbing/apex/wiki/Getting%20Started)
  guide to learn how to get APEX and how to play with it in Harvey.
- Take a look at
  [Bitbucket Contribution Guide](https://zapier.com/apps/bitbucket/tutorials/bitbucket-pull-request)
  through Pull Request system. Also, please, consider some
  inheritance from Harvey about rules, conventions, coding style,
  [Code of Conduct](https://github.com/Harvey-OS/harvey/wiki/Code-of-Conduct),
  where to get help and so on.
- We also cover a wide variety of topics on our
  [wiki](https://bitbucket.org/elbing/apex/wiki/Home).
- If the information you are looking for is not on the wiki, ask the
  [Mailing list](https://groups.google.com/forum/#!forum/harvey) or
  [Slack](https://harvey-slack.herokuapp.com/) for
  help. It is where most of the development conversation happens.

